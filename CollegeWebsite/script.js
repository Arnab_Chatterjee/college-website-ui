// Automobile 

let mission1 = document.getElementById("anchor1");
let vision1 = document.getElementById("anchor2");
let PEO1 = document.getElementById("anchor3");
let PO1 = document.getElementById("anchor4");
let heading1 = document.querySelector("h1");
let paragraph1 = document.querySelector("p");

vision1.addEventListener("click", function(){
     heading1.innerText =  "Departmental Vision";
     paragraph1.innerText = "To become a Center of Excellence in the Automobile Engineering discipline with a strong teaching environment that adapts swiftly to the challenges of the 21st century."
});

mission1.addEventListener("click", function(){
     heading1.innerText =  "Departmental Mission";
     paragraph1.innerHTML = "<p>To provide qualitative education and generate new knowledge by engaging in modern technology and by offering state-of-the-art diploma engineering program, leading to careers as professionals in the widely diversified domains of industry, government and academia. <br> <br> To promote a teaching and learning process that yields advancements in state-of-the-art in Automobile Engineering. <br> <br> To harness human capital for sustainable competitive edge and social relevance by inculcating the philosophy of continuous learning and innovation in Automobile Engineering.</p>";
});

PEO1.addEventListener("click", function(){
     heading1.innerText =  "Program Educational Objectives(PEO)";
     paragraph1.innerHTML = "<p>PEO-1: Diploma Engineers are prepared to be employed in core Industries and be engaged in learning, understanding and applying new ideas. <br> <br> PEO-2: Diploma Engineers are prepared to take up Graduate programmes. <br> <br> PEO-3: To equip graduates with integrity and ethical values so that they become responsible Engineers.</p>"
});

PO1.addEventListener("click", function(){
     heading1.innerText =  "Program Outcome(PO)";
     paragraph1.innerHTML = "<p>PO-1: Strong foundation in core Automobile Engineering and Technology, both theoretical and applied concepts. <br> PO-2: Ability to apply knowledge of mathematics, science, and engineering to real-life problem solving. <br> PO-3: Ability to analyze, design, model, and develop projects. <br> PO-4: Ability to function effectively within teams. <br> PO-5: Understanding of professional ethical responsibility. <br> PO-6: Ability to communicate effectively, both in writing and oral. <br> PO-7: Understanding the impact of Automobile Engineering solutions in the social and human Context. <br> PO-8: Ability to engage in life-long learning. <br> PO-9: Knowledge of contemporary issues</p>"
});

// Civil 

let mission2 = document.getElementById("anchor1");
let vision2 = document.getElementById("anchor2");
let PEO2 = document.getElementById("anchor3");
let PO2 = document.getElementById("anchor4");
let heading2 = document.querySelector("h1");
let paragraph2 = document.querySelector("p");

vision2.addEventListener("click", function(){
     heading2.innerText =  "Departmental Vision";
     paragraph2.innerText = "To excel in technical educational area of Civil Engineering and to provide expert, proficient and knowledgeable Civil Engineering individuals with high enthusiasm to meet the societal challenges."
});

mission2.addEventListener("click", function(){
     heading2.innerText =  "Departmental Mission";
     paragraph2.innerHTML = "<p>To provide an open environment to the students and faculty that promotes professional and personal growth.<br><br>To impart strong theoretical and practical background across the Civil Engineering discipline with an emphasis on supervising of constructional work, lab works and workshop.<br><br>To inculcate the skills necessary to continue their education after diploma, as well as for the socialneeds.</p>";
});

PEO2.addEventListener("click", function(){
     heading2.innerText =  "Program Educational Objectives(PEO)";
     paragraph2.innerHTML = "<p>PEO-1: Gain successful professional career in constructional industry as an efficient Civil engineer. <br> <br> PEO-2: Succeed in graduation programmes to gain knowledge on emerging technologies in Civil Engineering.<br> <br>PEO-3: Grow as a responsible constructional professional in their own area of interest with moral character, intellectual skills and ethics through lifelong learning approach to meet societal needs.</p>"
});

PO2.addEventListener("click", function(){
     heading2.innerText =  "Program Outcome(PO)";
     paragraph2.innerHTML = "<p>PO-1: Survey the land where the proposed Civil Engineering structure will be constructed. <br> PO-2: Apply the knowledge of draftsman-ship using related computer-based application software. <br>PO-3: Provide know-how on estimating & costing, budgeting, tendering and global tendering using related computer-based application software.<br>PO-4: Construct a fire-protected and well ventilated construction to meet societal needs.<br>PO-5: Apply appropriate techniques with the use of available resources and machines to construct various Civil Engineering structures.<br>PO-6: Apply knowledge on professional and ethical responsibilities.<br>PO-7: Understanding the impact of Automobile Engineering solutions in the social and human Context.<br> PO-8: Construct the Civil Engineering construction in systematic way using PERT/CPM, BAR CHART</p>"
});

// CSE 

let mission3 = document.getElementById("anchor1");
let vision3 = document.getElementById("anchor2");
let PEO3 = document.getElementById("anchor3");
let PO3 = document.getElementById("anchor4");
let heading3 = document.querySelector("h1");
let paragraph3 = document.querySelector("p");

vision3.addEventListener("click", function(){
     heading3.innerText =  "Departmental Vision";
     paragraph3.innerText = "To become a Center of Excellence in the computer sciences and Technology discipline with a strong teaching environment that adapts swiftly to the challenges of the 21st century."
});

mission3.addEventListener("click", function(){
     heading3.innerText =  "Departmental Mission";
     paragraph3.innerHTML = "<p>To provide qualitative education and generate new knowledge by engaging in cutting-edge technology and by offering state-of-the-art diploma engineering program, leading to careers as Computer and IT professionals in the widely diversified domains of industry, government and academia. <br> <br>To promote a teaching and learning process that yields advancements in state-of-the-art in computer science and technology. <br> <br>To harness human capital for sustainable competitive edge and social relevance by inculcating the philosophy of continuous learning and innovation in Computer Science</p>";
});

PEO3.addEventListener("click", function(){
     heading3.innerText =  "Program Educational Objectives(PEO)";
     paragraph3.innerHTML = "<p>PEO-1:Diploma Engineers are prepared to be employed in IT industries and be engaged in learning, understanding and applying new Ideas. <br> <br>PEO-2: Diploma Engineers are prepared to take up Graduate programmes.<br> <br>PEO-3: To equip graduates with integrity and ethical values so that they become responsible Engineers</p>"
});

PO3.addEventListener("click", function(){
     heading3.innerText =  "Program Outcome(PO)";
     paragraph3.innerHTML = "<p>PO-1: Strong foundation in core Computer Science and Technology, both theoretical and applied concepts.<br>PO-2: Ability to apply knowledge of mathematics, science, and engineering to real-life problem solving.<br>PO-3: Ability to analyze, design, model, and develop complex software and information management systems.<br>PO-4: Ability to function effectively within teams.<br>PO-5: Understanding of professional ethical responsibility.<br>PO-6: Ability to communicate effectively, both in writing and oral.<br>PO-7: Understanding the impact of Computer Science and Technology solutions in the societal and human context.<br>PO-8: Ability to engage in life-long learning.<br> PO-9: Knowledge of contemporary issues</p>"
});

// EE 

let mission4 = document.getElementById("anchor1");
let vision4 = document.getElementById("anchor2");
let PEO4 = document.getElementById("anchor3");
let PO4 = document.getElementById("anchor4");
let heading4 = document.querySelector("h1");
let paragraph4 = document.querySelector("p");

vision4.addEventListener("click", function(){
     heading4.innerText =  "Departmental Vision";
     paragraph4.innerText = "To organize a learning environment where students will be inquisitive to become future engineers and pioneering technologists. To inculcate acumen of enhanced technical learning."
});

mission4.addEventListener("click", function(){
     heading4.innerText =  "Departmental Mission";
     paragraph4.innerHTML = "<p>Enabling students for more creative and innovative thinking.<br> <br> Enabling students to explore emerging field of engineering and their possible career options.<br> <br>To inculcate sincerity and truthfulness through curricular, co-curricular and extra-curricular behavior.</p>";
});

PEO4.addEventListener("click", function(){
     heading4.innerText =  "Program Educational Objectives(PEO)";
     paragraph4.innerHTML = "<p>PEO-1: Students from our institute will be dynamic in the proficient practice of electrical engineering and associated fields. They will acquire employment suitable to their background and education and will proceed in their profession. <br> <br>PEO-2: Students from our institute will connect in enduring knowledge established by sophisticated teaching, expert development actions, and/or additional career-appropriate options.<br> <br>PEO-3: Our Students working within technical fields will reveal scientific proficiency in identifying, formulating, analyzing, and creating engineering solutions.<br> <br>PEO-4: Accomplish the requirements of civilization in solving technical troubles using engineering ideology, apparatus and practices, in an ethical and responsible approach</p>"
});

PO4.addEventListener("click", function(){
     heading4.innerText =  "Program Outcome(PO)";
     paragraph4.innerHTML = "<p>PO-1: To develop aptitude to relate knowledge of mathematics, science and engineering for the resolution of electrical engineering problems.<br>PO-2: To develop skill to prepare and analyze complicated electrical engineering problems.<br> PO-3: To develop Ability to propose a system, module, or process to meet required needs within realistic constraints such as profitable, environmental, community, and civic health.<br>PO-4: Ability to design and conduct experiments, and to analyze and interpret technical data.<br>PO-5: To develop capability to use the techniques, skills, and contemporary engineering tools essential for electrical engineering practice.<br>PO-6: To develop capability to embrace social, intellectual, ethical issues with engineering solutions.<br>PO-7: To develop capability to analyze the impact of engineering solutions on environment and the urgency for sustainable development.<br>PO-8:To develop capability to work effectively on multidisciplinary teams.<br>PO-9: To develop capability to communicate successfully.<br>PO-10: To develop comprehensive understanding and awareness of management philosophy and finance related to engineering business.</p>"
});

// ECE 

let mission5 = document.getElementById("anchor1");
let vision5 = document.getElementById("anchor2");
let PEO5 = document.getElementById("anchor3");
let PO5 = document.getElementById("anchor4");
let heading5 = document.querySelector("h1");
let paragraph5 = document.querySelector("p");

vision5.addEventListener("click", function(){
     heading5.innerText =  "Departmental Vision";
     paragraph5.innerText = "To strive excellence in education , research and entrepreneurship and to create technical manpower of global standard with capabilities of facing new challenges and of offering scientific and technological services to society."
});

mission5.addEventListener("click", function(){
     heading5.innerText =  "Departmental Mission";
     paragraph5.innerHTML = "<p>To create competent professionals, through quality education in the area and Telecommunication Engineering, who can contribute in the advancement of science technology. <br> <br>To create professionals who can accept new challenges in science and technology and emerge successfully in global competitive scenario.<br> <br>To impart value based education which will result in services and entrepreneurial initiatives towards socio-economic development of society.<br> <br>To provide support to promote research and development activities in the field of Electronics and Telecommunication Engineering and in allied fields.</p>";
});

PEO5.addEventListener("click", function(){
     heading5.innerText =  "Program Educational Objectives(PEO)";
     paragraph5.innerHTML = "<p>PEO-1: Identify, define and solve problems in the field with analytical abilities using acquired knowledge in mathematical, scientific and engineering fundamentals.<br> <br>PEO-2: Advance in Industry/ technical profession, effectively in a dynamic global environment, pursue higher studies and engage themselves in research and development of the society.<br> <br>PEO-3: Inculcate and embrace professional and ethical attitudes, effective communication skills, spirit of teamwork, multi-disciplinary approach, quest for life-long learning and concern for the society.</p>"
});

PO5.addEventListener("click", function(){
     heading5.innerText =  "Program Outcome(PO)";
     paragraph5.innerHTML = "<p>PO-1: Formal lectures in conventional way.<br> PO-2: Experimental support in labs<br>PO-3: Training in industry.<br> PO-4: Support to widen Engineering knowledge based through deliberation by expert other than faculty<br> PO-5: Extensive tutorial work/activity<br>PO-6: Numerical problem solving session.<br>PO-7: Assignment generally of design based work<br>PO-8:Project work activity.<br>PO-9: Initiation in social service in group and collaboration.<br>PO-10:Seminar and group discussion session</p>"
});

// HM 

let mission6 = document.getElementById("anchor1");
let vision6 = document.getElementById("anchor2");
let heading6 = document.querySelector("h1");
let paragraph6 = document.querySelector("p");

vision6.addEventListener("click", function(){
     heading6.innerText =  "Departmental Vision";
     paragraph6.innerText = "To emerge as a leading educational institute in the field of Hospitality management Education and to be reckoned as one of the top Hotel management Institute nationwide, by providing Quality Education and making excellent Industry-Institute Interface, consequently to achieve superior placement record."; 
});

mission6.addEventListener("click", function(){
     heading6.innerText =  "Departmental Mission";
     paragraph6.innerHTML = "<p> To assist all Elittians in being well-equipped with the requisite information and skills, empowering them to become the widely recognized professionals inside the global market.<br> <br>To make the value addition in the knowledge domain as per the industry standards.<br> <br>Re-energizing the global market scenario in concern with the later alterations.<br> <br>To reinstate the eternal aesthetics emancipating the intrinsic concept of the team 'HOSPITALITY' in the global arena</p>";
});

// ME 

let mission7 = document.getElementById("anchor1");
let vision7 = document.getElementById("anchor2");
let PEO7 = document.getElementById("anchor3");
let PO7 = document.getElementById("anchor4");
let heading7 = document.querySelector("h1");
let paragraph7 = document.querySelector("p");

vision7.addEventListener("click", function(){
     heading7.innerText =  "Departmental Vision";
     paragraph7.innerText = "To become a Center of Excellence in the Mechanical Engineering discipline with a strong teaching environment that adapts swiftly to the challenges of the 21st century."
});

mission7.addEventListener("click", function(){
     heading7.innerText =  "Departmental Mission";
     paragraph7.innerHTML = "<p> To provide qualitative education and generate new knowledge by engaging in modern technology and by offering state-of-the-art diploma engineering program, leading to careers as professionals in the widely diversified domains of industry, government and academia.<br> <br> To promote a teaching and learning process that yields advancements in state-of-the-art in Mechanical Engineering. <br> <br>To harness human capital for sustainable competitive edge and social relevance by inculcating the philosophy of continuous learning and innovation in Mechanical Engineering.</p>";
});

PEO7.addEventListener("click", function(){
     heading7.innerText =  "Program Educational Objectives(PEO)";
     paragraph7.innerHTML = "<p>PEO-1: Diploma Engineers are prepared to be employed in core Industries and be engaged in learning, understanding and applying new ideas.<br> <br> PEO-2: Diploma Engineers are prepared to take up Graduate programmes.<br><br> PEO-3: To equip graduates with integrity and ethical values so that they become responsible Engineers.</p>"
});

PO7.addEventListener("click", function(){
     heading7.innerText =  "Program Outcome(PO)";
     paragraph7.innerHTML = "<p>PO-1: Strong foundation in core Mechanical Engineering and Technology, both theoretical and applied concepts.<br>PO-2: Ability to apply knowledge of mathematics, science, and engineering to real-life problem solving.<br>PO-3: Ability to analyze, design, model, and develop projects .<br>PO-4: Ability to function effectively within teams.<br>PO-5: Understanding of professional ethical responsibility.<br>PO-6: Ability to communicate effectively, both in writing and oral.<br>PO-7: Understanding the impact of Mechanical Engineering solutions in the social and human Context.<br>PO-8:Ability to engage in life-long learning.<br>PO-9: Knowledge of contemporary issues</p>"
});

// Sh 

let mission8 = document.getElementById("anchor1");
let vision8 = document.getElementById("anchor2");
let PEO8 = document.getElementById("anchor3");
let PO8 = document.getElementById("anchor4");
let heading8 = document.querySelector("h1");
let paragraph8 = document.querySelector("p");

vision8.addEventListener("click", function(){
     heading8.innerText =  "Departmental Vision";
     paragraph8.innerText = "To establish a centre par excellence in the area of basic sciences such as Mathematics, Physics, Chemistry and English Language & Communication; to share the knowledge, skill and understanding of subjects that provide foundation for engineering studies and also to empower the students expressing effectively in the era of advanced technology and globalization; to create proficient engineers who can face challenges across-the- board in engineering fundamentals - experimental, analytical, computational and designing abilities."
});

mission8.addEventListener("click", function(){
     heading8.innerText =  "Departmental Mission";
     paragraph8.innerHTML = "<p> To provide the students a congenial establishment in mathematical, scientific and engineering fundamentals essential to recognize, evaluate and decide real life troubles. To develop excellent written and oral communication skills, including presentation skills and technical writing for effectively interacting with clients, customers, co-workers and managers. To contribute to elevate the quality of technical education while producing dynamic engineers and entrepreneurs for the country.</p>";
});

PEO8.addEventListener("click", function(){
     heading8.innerText =  "Program Educational Objectives(PEO)";
     paragraph8.innerHTML = "<p>PEO-1: Facilitate learning of the strong foundation of Basic and Engineering Science, Mathematics, Data reduction, error analysis & computation, communication & presentation skills, Team work, and Leadership.<br> <br>PEO-2: Develop adequate background and options for pursuing diverse career paths in engineering disciplines in the context of community, country and economy.<br> <br>PEO-3: Develop self-learning skills in a volatile socio-psycho-somatic environment and physiological / hormonal changes in the body. Encourage and nurture the value of independent lifelong learning.<br> <br>PEO-4:Develop awareness of converging technologies and sciences such as Nanotechnology, Biotechnology etc. and equip them with abilities to contribute multidisciplinary, multicultural, and multinational environment.<br> <br>PEO-5:Inspire to equip with necessary information and skills to pursue higher studies in India and abroad.</p>"
});

PO8.addEventListener("click", function(){
     heading8.innerText =  "Program Outcome(PO)";
     paragraph8.innerHTML = "<p>PO-1: An ability to apply knowledge of mathematics, science and engineering.<br>PO-2: An ability to identify, analyze and specify a technical community problem.<br> PO-3:To design and conduct experiments, analyze and interpretation of data.<br>PO-4: To understand the impact of engineering solutions in a global, economic, environmental and societal context.<br> PO-5: To acquire communicative language skills, professional, managerial, ethical and environmental based principles to become the face of our institution in leading organizations.<br>PO-6: To design a system, component or process to cater the needs of economics, environment, social, political, ethical, health and safety.<br> PO-7: Addressing the problems associated in the interface of Theoretical knowledge, understanding and application, skill.<br>PO-8:An ability to function on a multidisciplinary team.<br>PO-9: An ability to engage in life-long learning ability</p>"
});